# Python 추상 클래스 및 인터페이스 <sup>[1](#footnote_1)</sup>

<font size="3">Python OOP에서 계약을 정의할 때 추상 클래스와 인터페이스를 사용하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./abstract-classes-and-interfaces.md#intro)
1. [추상 클래스 및 인터페이스란?](./abstract-classes-and-interfaces.md#sec_02)
1. [Python에서 추상 클래스를 정의하고 사용하는 방법](./abstract-classes-and-interfaces.md#sec_03)
1. [Python에서 인터페이스를 정의하고 사용하는 방법](./abstract-classes-and-interfaces.md#sec_04)
1. [추상 클래스 대 인터페이스: 언제 무엇을 사용할까?](./abstract-classes-and-interfaces.md#sec_05)
1. [요약](./abstract-classes-and-interfaces.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 24 — Python Abstract Classes and Interfaces](https://python.plainenglish.io/python-tutorial-24-python-abstract-classes-and-interfaces-2769570a61c0)를 편역하였다.
