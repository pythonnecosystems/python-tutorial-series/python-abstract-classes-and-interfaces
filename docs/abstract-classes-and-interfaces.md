# Python 추상 클래스 및 인터페이스

## <a name="intro"></a> 개요
Python 추상 클래스와 인터페이스에 대한 이 포스팅에서는 다음과 같은 내용을 배울 수 있다.

- 추상 클래스와 인터페이스는 무엇이며 Python 객체 지향 프로그래밍(OOP)에서 계약을 정의하는 데 왜 유용할까?
- `abc` 모듈을 사용하여 Python에서 추상 클래스를 정의하고 사용하는 방법
- `zope.interface` 모듈을 사용하여 Python에서 인터페이스를 정의하고 사용하는 방법
- 추상 클래스와 인터페이스를 비교하고 대조하는 방법과 Python 프로젝트에서의 선택 기준

이 글을 읽고나면 여러분은 강력하고 일관된 Python 프로그램을 설계하고 구현하기 위해 추상 클래스와 인터페이스를 사용하는 방법에 대해 잘 이해할 수 있을 것이다.

시작한다!

## <a name="sec_02"></a> 추상 클래스 및 인터페이스란?
이 섹션에서는 추상 클래스와 인터페이스가 무엇이며 Python OOP에서 계약을 정의하는 데 유용한 이유에 대해 알아본다.

추상 클래스와 인터페이스는 구현 세부 사항을 지정하지 않고 클래스가 구현해야 하는 일련의 방법과 속성을 정의할 수 있는 두 가지 개념이다. 추상 클래스와 인터페이스는 클래스에 대한 일관된 동작과 구조를 설계하고 적용하는 데 유용할 뿐만 아니라 다형성과 코드 재사용을 촉진하는 데 유용하다.

추상 클래스는 하나 이상의 추상 메서드를 포함하는 클래스로, 본체가 없고 `@abstractmethod` 데코레이터로 표시된 메서드이다. 추상 클래스는 인스턴스화할 수 없지만 추상 메서드에 대한 구현을 제공하는 다른 클래스로 상속할 수 있다. 추상 클래스는 또한 하위 클래스로 상속되는 구체적인 메서드와 속성을 포함할 수 있다.

인터페이스는 추상 메서드만 포함하고 속성은 포함하지 않는 클래스이다. 인터페이스는 인스턴스화되거나 상속될 수 없지만 모든 추상 메서드에 대한 구현을 제공하는 다른 클래스에서 구현할 수 있다. 인터페이스는 다른 인터페이스에서도 상속되어 인터페이스의 계층 구조를 만들 수 있다. 인터페이스는 상속 관계를 부과하지 않고 클래스가 따라야 하는 계약을 지정하는 방법이다.

Python에서는 인터페이스에 대한 내장 지원이 없지만 `zope.interface` 모듈을 사용하여 Python 프로그램에서 인터페이스를 정의하고 사용할 수 있다. `zope.interface` 모듈은 인터페이스를 선언하고 클래스가 올바르게 구현되었는지 확인하는 방법을 제공한다.

[다음 섹션](#sec_03)에서는 몇 가지 예를 들어 Python에서 추상 클래스 및 인터페이스를 정의하고 사용하는 방법을 보인다.

## <a name="sec_03"></a> Python에서 추상 클래스를 정의하고 사용하는 방법
이 절에서는 `abc` 모듈을 사용하여 Python에서 추상 클래스를 정의하고 사용하는 방법을 배울 것이다. `abc` 모듈은 추상 기저 클래스(abstract base class)를 만들고 하위 클래스를 등록하는 도구를 제공한다.

Python에서 추상 클래스를 정의하려면 다음 단계를 수행해야 한다.

1. `abc` 모듈을 임포트한다.
1. `abc` 모듈에서 추상 클래스의 기저 클래스(base class)인 `ABC` 클래스를 상속하는 클래스를 만든다.
1. 추상적으로 만들 메소드를 표시하려면 `@abstractmethod` 데코레이터를 사용한다. 추상 메소드는 몸체가 없고 하위 클래스에 의해 오버라이드(override)되어야 하는 메소드이다.
1. 또는 `@abstractproperty` 데코레이터를 사용하여 추상화할 속성을 표시할 수도 있다. 추상 속성은 getter 또는 setter가 없는 속성으로 하위 클래스에서 정의되어야 한다.

여기에 `sound`와 `move`라는 두 가지 추상 메서드와 `name`이라는 하나의 추상 속성을 가진 `Animal`이라는 추상 클래스를 정의하는 방법의 예가 있다.

```python
# Import the abc module
from abc import ABC, abstractmethod, abstractproperty
# Define an abstract class called Animal
class Animal(ABC):
    # Define an abstract property called name
    @abstractproperty
    def name(self):
        pass
    # Define an abstract method called sound
    @abstractmethod
    def sound(self):
        pass
    # Define an abstract method called move
    @abstractmethod
    def move(self):
        pass
```

Python에서 추상 클래스를 사용하려면 다음 단계를 수행해야 한다.

1. 추상 클래스에서 상속되는 하위 클래스를 만든다.
1. 하위 클래스의 모든 추상 메서드와 속성에 대한 구현을 제공한다. 그렇지 않으면 하위 클래스도 추상적이어서 인스턴스화할 수 없다.
1. 하위 클래스의 동작을 변경하거나 새 기능을 추가하려는 경우 하위 클래스의 구체적인 메서드와 속성을 재정의할 수도 있다.
1. 하위 클래스의 인스턴스를 만들고 해당 메서드와 속성을 평소와 같이 사용한다.

추상 메서드와 속성을 구현하는 `Dog`와 `Cat`의 두 가지 하위 클래스를 만들어 추상 클래스 `Animal`을 사용하는 방법을 예로 들어보겠다.

```python
# Define a subclass called Dog that inherits from Animal
class Dog(Animal):
    # Define the name property for Dog
    @property
    def name(self):
        return "Dog"
    # Define the sound method for Dog
    def sound(self):
        print("Woof!")
    # Define the move method for Dog
    def move(self):
        print("Run!")
# Define a subclass called Cat that inherits from Animal
class Cat(Animal):
    # Define the name property for Cat
    @property
    def name(self):
        return "Cat"
    # Define the sound method for Cat
    def sound(self):
        print("Meow!")
    # Define the move method for Cat
    def move(self):
        print("Jump!")
# Create an instance of Dog and use its methods and properties
dog = Dog()
print(dog.name) # Dog
dog.sound() # Woof!
dog.move() # Run!
# Create an instance of Cat and use its methods and properties
cat = Cat()
print(cat.name) # Cat
cat.sound() # Meow!
cat.move() # Jump!
```

보다시피 추상 클래스 `Animal`은 추상메서드와 속성을 구현하여 하위 클래스인 `Dog`와 `Cat`이 따라야 할 계약을 정의한다. 이를 통해 `Animal`의 모든 하위 클래스가 일관된 행동과 구조를 갖추고 상호 교환하여 사용할 수 있다.

## <a name="sec_04"></a> Python에서 인터페이스를 정의하고 사용하는 방법
이 절에서는 `zope.interface` 모듈을 사용하여 Python에서 인터페이스를 정의하고 사용하는 방법을 배울 것이다. `zope.interface` 모듈은 인터페이스를 만들고 검증하는 도구와 그 구현을 제공한다.

Python에서 인터페이스를 정의하려면 다음 단계를 수행해야 한다.

1. `pip` 또는 다른 패키지 관리자를 사용하여 `zope.interface` 모듈을 설치한다.
1. `zope.interface` 모듈에서 `Interface` 클래스와 `implementor` 데코레이터를 임포트한다.
1. `zope.interface` 모듈의 인터페이스에 대한 기저 클래스인 `Interface` 클래스로부터 상속되는 클래스를 만든다.
1. 추상적으로 만들 메소드를 표시하려면 `@abstractmethod` 데코레이터를 사용한다. 추상 메소드는 바디가 없고 인터페이스를 구현하는 클래스에서 구현해야 하는 메소드이다.
1. 또는 `@invariant` 데코레이터를 사용하여 인터페이스 구현의 유효성을 검사하는 불변 함수를 정의할 수도 있다. 불변 함수는 인터페이스 구현의 인스턴스를 인수로 간주하고 인스턴스가 인터페이스 계약을 만족하지 않으면 예외를 발생시키는 함수이다.

다음은 추상 메서드인 `fly`와 불변 함수인 `check_wings`를 가진 `IFlyable`이라는 인터페이스를 정의하는 방법의 예이다.

```python
# Install the zope.interface module
# pip install zope.interface
# Import the Interface class and the implementer decorator
from zope.interface import Interface, implementer, abstractmethod, invariant
# Define an interface called IFlyable
class IFlyable(Interface):
    # Define an abstract method called fly
    @abstractmethod
    def fly(self):
        pass
    # Define an invariant function called check_wings
    @invariant
    def check_wings(obj):
        if obj.wings < 2:
            raise Exception("A flyable object must have at least two wings")
```

Python에서 인터페이스를 사용하려면 다음 단계를 수행해야 한다.

1. 인터페이스의 모든 추상 메서드에 대한 구현을 제공하여 인터페이스를 구현하는 클래스를 만든다.
1. `@implementer` 데토레이터를 사용하여 클래스를 인터페이스 구현으로 표시한다. 데코레이터는 인터페이스 클래스를 인수로 받아 클래스가 인터페이스를 올바르게 구현하는지 확인한다.
1. 클래스의 인스턴스를 만들고 해당 메서드를 평소와 같이 사용한다.
1. 선택적으로 `zope.interface.verify` 모듈에서 `verifyObject` 함수를 사용하여 클래스의 인스턴스가 런타임에 인터페이스를 준수하는지 확인할 수도 있다. 이 함수는 인터페이스 클래스와 인스턴스를 인수로 가져와 인스턴스가 인터페이스를 구현하거나 예외를 발생시키면 `True`를 반환한다.

다음은 인터페이스를 구현하는 `Bird`와 `Airplane` 두 클래스를 만들어 `Iflyable` 인터페이스를 사용하는 방법의 예이다.

```python
# Import the verifyObject function
from zope.interface.verify import verifyObject
# Define a class called Bird that implements IFlyable
@implementer(IFlyable)
class Bird:
    # Define the constructor that takes the number of wings as an argument
    def __init__(self, wings):
        self.wings = wings
    # Define the fly method for Bird
    def fly(self):
        print("Flap wings!")
# Define a class called Airplane that implements IFlyable
@implementer(IFlyable)
class Airplane:
    # Define the constructor that takes the number of wings as an argument
    def __init__(self, wings):
        self.wings = wings
    # Define the fly method for Airplane
    def fly(self):
        print("Start engine!")
# Create an instance of Bird and use its methods
bird = Bird(2)
print(bird.wings) # 2
bird.fly() # Flap wings!
# Create an instance of Airplane and use its methods
airplane = Airplane(2)
print(airplane.wings) # 2
airplane.fly() # Start engine!
# Verify that bird and airplane implement IFlyable
print(verifyObject(IFlyable, bird)) # True
print(verifyObject(IFlyable, airplane)) # True
```

보다시피, 인터페이스 `IFlyable`은 추상 메소드 `fly`를 구현하여 클래스 `Bird`와 `Airplane`이 따라야 하는 계약을 정의한다. 인터페이스는 또한 인터페이스 구현의 유효성을 검사하는 불변 함수 `check_wings`를 정의한다. `@implementer` 데코레이터와 `verifyObject` 함수는 클래스가 인터페이스를 바르게 구현하고 인터페이스 계약에 부합하는지 확인한다.

## <a name="sec_05"></a> 추상 클래스 대 인터페이스: 언제 무엇을 사용할까?
이 절에서는 추상 클래스와 인터페이스를 비교와 대조하는 방법과 Python 프로젝트에 어떤 인터페이스를 사용할 지에 대해 알아본다.

추상 클래스와 인터페이스는 모두 Python OOP에서 계약을 정의하는 데 유용하지만, 둘 중 하나를 선택하기 전에 고려해야 할 몇 가지 차이점과 장점이 있다.

추상 클래스와 인터페이스의 주요 차이점과 장점은 다음과 같다.

- **상속(Inheritance) 대 구현(Implementation)**: 추상 클래스는 상속을 기반으로 하는데, 이는 하위 클래스가 추상 클래스에서 상속하고 추상 메서드와 속성에 대한 구현을 제공한다는 것을 의미한다. 인터페이스는 구현을 기반으로 하는데, 이는 클래스가 인터페이스를 구현하고 인터페이스의 모든 추상 메서드에 대한 구현을 제공한다는 것을 의미한다. 상속은 "is-a" 관계를 의미하는 반면, 구현은 "has-a" 관계를 의미한다. 예를 들어, `Dog`는 `Animal`이지만 `Airplane`는 `IFlyable` 기능을 가지고 있다.
- **단일(Single) 대 다중(Multiple)**: 추상 클래스는 단일 상속 원칙을 따르므로 하위 클래스가 하나의 추상 클래스에서만 상속받을 수 있다. 인터페이스는 다중 상속 원칙을 따르므로 클래스가 여러 인터페이스를 구현할 수 있다. 이를 통해 서로 다른 인터페이스를 결합하여 클래스에 대해 서로 다른 동작을 정의할 수 있으므로 보다 유연하고 모듈식 설계를 만들 수 있다. 예를 들어 `Bat`은 `IFlyable` 인터페이스와 `IMammal` 인터페이스를 모두 구현할 수 있지만 `Fish`는 `IMammal` 인터페이스만 구현할 수 있다.
- **구체적(Concrete) 대 추상적(Abstract)**: 추상 클래스는 추상적이고 구체적인 메소드와 속성을 모두 포함할 수 있으며, 이는 하위 클래스에 기본 구현을 제공할 수 있음을 의미한다. 인터페이스는 추상적 메소드만 포함할 수 있으며 속성은 포함하지 않을 수 있으며, 이는 인터페이스가 구현을 제공하지 않고 계약만 지정할 수 있음을 의미한다. 이는 인터페이스를 더욱 추상적이고 순수하게 만들지만, 또한 더욱 제한적이고 재사용성이 떨어진다. 예를 들어, 추상 클래스 `Animal`은 "Eat food!"를 인쇄하는 구체적인 메소드 `eat`를 제공할 수 있는 반면, 인터페이스 `IFlyable`은 인터페이스를 구현하는 클래스에 의해 구현되어야 하는 추상적 메소드 `fly`만을 제공할 수 있다.

그렇다면 Python 프로젝트에 어느 경우 추상 클래스를 사용하고 어느 경우 인터페이스를 사용해야 할까?

프로젝트의 설계와 요구 사항에 따라 다르기 때문에 이 질문에 대한 확실한 답은 없다. 그러나 결정에 도움이 될 수 있는 몇 가지 일반적인 지침이 있다.

- "is-a" 관계를 공유하는 관련 클래스 그룹에 대한 공통 행동 및 구조를 정의하고자 할 때 추상 클래스를 사용한다. 예를 들어 추상 클래스 `Animal`을 사용하여 `Dog`, `Cat`, `Bird` 등 관련 클래스 그룹에 대한 공통 행동 및 구조를 정의한다.
- "has-a" 관계를 공유하는 관련 없는 클래스 그룹에 대한 특정 기능 또는 동작을 정의하고자 할 때 인터페이스를 사용한다. 예를 들어, `Bird`, `Airplane`, `Bat` 등 관련 없는 클래스 그룹에 대한 특정 기능 또는 동작을 정의하기 위해 `IFlyable` 인터페이스를 사용한다.
- 두 개념의 장점을 결합하고 보다 유연하고 모듈식 디자인을 만들고 싶을 때 추상 클래스와 인터페이스를 모두 사용한다. 예를 들어 추상 클래스 `Animal`과 인터페이스 `IFlyable`을 사용하여 `Animal`에서 상속받고 `IFlyable`을 구현하는 하위 클래스 `Bird`를 만든다.

결론적으로 추상 클래스와 인터페이스는 Python OOP에서 계약을 정의하는 강력한 도구이지만 사용하기 전에 고려해야 할 목적과 장점이 다르다. 추상 클래스와 인터페이스의 차이점과 장점을 이해함으로써 Python 프로젝트에 가장 적합한 옵션을 선택하고 보다 강력하고 일관된 프로그램을 만들 수 있다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python OOP에서 계약을 정의할 때 추상 클래스와 인터페이스를 사용하는 방법을 배웠다.

- 추상 클래스 및 인터페이스는 무엇이며 Python OOP에서 계약을 정의하는 데 왜 유용한가?
- `abc` 모듈을 사용하여 Python에서 추상 클래스를 정의하고 사용하는 방법.
- `zope.interface` 모듈을 사용하여 Python에서 인터페이스를 정의하고 사용하는 방법.
- 추상 클래스와 인터페이스를 비교하고 대조하는 방법과 Python 프로젝트에 어떤 것을 사용할 것인지를 알아 보았다.

이 포스팅을 이해했다면 강력하고 일관된 Python 프로그램을 설계하고 구현하기 위해 추상 클래스와 인터페이스를 잘 사용할 수 있게 되었을 것이다.
